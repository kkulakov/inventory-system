package inventorysystem

import grails.transaction.Transactional
import inventorysystem.enums.Repeat
import inventorysystem.utils.EventJSONTemplate
import org.codehaus.groovy.grails.web.mapping.LinkGenerator

@Transactional
class EventService {
    LinkGenerator grailsLinkGenerator

    List<EventJSONTemplate> getEventsForYear(MerchType merchType, String description) {
        List<EventJSONTemplate> result = new ArrayList<>();

        switch(merchType.repeat){
            case Repeat.DAY_INTERVAL:
                Date date = new Date().clearTime();    //TODO: fix this line
                int nextyear = date.year + 1
                while(date.year != nextyear) {
                    if(!merchType.daysInterval || merchType.daysInterval <= 0) break

                    result.add createEventJSONTemplate(merchType, date.time, description)

                    date = date.plus(merchType.daysInterval)
                }
                break
            case Repeat.MONTHLY:
                if(merchType.dates.size() != 1) break

                Date date = merchType.dates.iterator().next()
                int nextyear = new Date().year + 1
                while(date.year != nextyear) {
                    result.add createEventJSONTemplate(merchType, date.time, description)
                    date.month++
                }

                break
            case Repeat.YEARLY:
                if(merchType.dates.size() != 1) break

                Date resultDate = merchType.dates.iterator().next()
                resultDate.year = new Date().year

                result.add createEventJSONTemplate(merchType, resultDate.time, description)

                break
            case Repeat.DATES:
                merchType.dates.each {

                    result.add createEventJSONTemplate(merchType, it.time, description)
                }
                break
        }
        return result
    }

    private EventJSONTemplate createEventJSONTemplate(MerchType merchType, long date, String description){
        new EventJSONTemplate(
            date: date,
            title: merchType.name,
            description: description,
            url: grailsLinkGenerator.link(controller: 'merchType', action: 'show', id: merchType.id)
        )
    }
}
