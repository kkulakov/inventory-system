package inventorysystem.utils

class MerchTypeUtils {
    static def stringToDateSet(String str) {
        str.split(',').collect {new Date(it)}
    }
}
