package inventorysystem

class Outlet {

    String address
    String phoneNumber
    static hasMany = [merchTypes : MerchType]

    static constraints = {
        address (blank: false, unique: true)
        phoneNumber (nullable: true)
        merchTypes (nullable: true)
    }
}
