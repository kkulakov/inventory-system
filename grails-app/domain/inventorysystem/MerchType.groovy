package inventorysystem

import inventorysystem.enums.Repeat

class MerchType {

    String name
    Repeat repeat
    Integer daysInterval
    static hasMany = [dates: Date]

    static constraints = {
        name (blank: false, unique: true)
        repeat (blank: false, enumType: 'ordinal')
        daysInterval nullable: true
        dates nullable: true
    }
}
