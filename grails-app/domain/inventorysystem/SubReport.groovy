package inventorysystem

class SubReport {

    MerchType merchType
    int supposedToBe
    int fact
    Date plannedDate
    Date realDate = new Date().clearTime()

    static constraints = {
    }
}
