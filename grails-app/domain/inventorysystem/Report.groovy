package inventorysystem

class Report {

    String responsiblePerson
    Outlet outlet
    static hasMany = [supReports: SubReport]

    static constraints = {
        responsiblePerson blank: false
    }
}
