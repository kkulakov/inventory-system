import inventorysystem.MerchType
import inventorysystem.User
import inventorysystem.Outlet
import inventorysystem.Role
import inventorysystem.UserRole
import inventorysystem.enums.Repeat

class BootStrap {

    def init = { servletContext ->
        Role userRole = Role.findByAuthority("ROLE_USER")
        if(!userRole) {
            userRole = new Role(authority: "ROLE_USER").save(flush: true)
        }

        Role managerRole = Role.findByAuthority("ROLE_MANAGER")
        if(!managerRole) {
            managerRole = new Role(authority: "ROLE_MANAGER").save(flush: true)
        }

        User user = User.findByEmail("test@test.com")
        if(!user) {
            user = new User(
                    email: "test@test.com",
                    password: "test",
                    firstName: "John",
                    lastName: "Doe"
            ).save(flush: true)
        }
        UserRole.create(user, userRole, true)

        MerchType merchType1 = MerchType.findByName("Клавіатура")
        if(!merchType1) {
            merchType1 = new MerchType(
                    name: "Клавіатура",
                    repeat: Repeat.DAY_INTERVAL,
                    daysInterval: 20
            ).save(flush: true)
        }

        MerchType merchType2 = MerchType.findByName("Монітор")
        if(!merchType2) {
            merchType2 = new MerchType(
                    name: "Монітор",
                    repeat: Repeat.DAY_INTERVAL,
                    daysInterval: 10
            ).save(flush: true)
        }

        MerchType merchType3 = MerchType.findByName("Принтер")
        if(!merchType3) {
            merchType3 = new MerchType(
                    name: "Принтер",
                    repeat: Repeat.MONTHLY
            )
            merchType3.addToDates(new Date())
            merchType3.save(flush: true)
        }

        Outlet outlet = Outlet.findByAddress("бул. Шевченка 77/2")
        if(!outlet) {
            outlet = new Outlet(
                    address: "бул. Шевченка 77/2",
                    phoneNumber: "123-45-67"
            )
            outlet.addToMerchTypes(merchType1)
            outlet.addToMerchTypes(merchType2)
            outlet.addToMerchTypes(merchType3)
            outlet.save(flush: true)
        }
    }
    def destroy = {
    }
}
