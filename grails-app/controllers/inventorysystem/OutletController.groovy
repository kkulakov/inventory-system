package inventorysystem



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class OutletController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Outlet.list(params), model: [outletInstanceCount: Outlet.count()]
    }

    def show(Outlet outletInstance) {
        respond outletInstance
    }

    def create() {
        respond new Outlet(params), model: [merchTypeList: MerchType.findAll()]
    }

    @Transactional
    def save(Outlet outletInstance) {
        params.list("merchType")?.each {outletInstance.addToMerchTypes(MerchType.get(it.toLong()))}
        if (outletInstance == null) {
            notFound()
            return
        }

        if (outletInstance.hasErrors()) {
            respond outletInstance.errors, view: 'create'
            return
        }

        outletInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'outletInstance.label', default: 'Outlet'), outletInstance.id])
                redirect outletInstance
            }
            '*' { respond outletInstance, [status: CREATED] }
        }
    }

    def edit(Outlet outletInstance) {
        respond outletInstance, model: [merchTypeList: MerchType.findAll()]
    }

    @Transactional
    def update(Outlet outletInstance) {
        outletInstance?.merchTypes?.clear()
        params.list("merchType")?.each {outletInstance.addToMerchTypes(MerchType.get(it.toLong()))}
        if (outletInstance == null) {
            notFound()
            return
        }

        if (outletInstance.hasErrors()) {
            respond outletInstance.errors, view: 'edit'
            return
        }

        outletInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Outlet.label', default: 'Outlet'), outletInstance.id])
                redirect outletInstance
            }
            '*' { respond outletInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Outlet outletInstance) {

        if (outletInstance == null) {
            notFound()
            return
        }

        outletInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Outlet.label', default: 'Outlet'), outletInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'outletInstance.label', default: 'Outlet'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
