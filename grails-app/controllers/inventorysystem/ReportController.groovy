package inventorysystem

import grails.plugin.springsecurity.SpringSecurityService
import grails.transaction.Transactional
import inventorysystem.utils.EventJSONTemplate

import java.text.SimpleDateFormat

@Transactional(readOnly = true)
class ReportController {
    static allowedMethods = [save: "POST"]

    SpringSecurityService springSecurityService
    EventService eventService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Report.list(params), model:[ReportInstanceCount: Report.count()]
    }

    def choose() {
        return [outletList: Outlet.list(), merchTypeList: MerchType.list()]
    }

    def create() {
        User user = springSecurityService.currentUser.asType(User.class)
        Outlet outlet = Outlet.get(params.long("outlet"))
        Report report = new Report(
                outlet: outlet,
                responsiblePerson: user.firstName + " " + user.lastName
        )
        List<Long> merchTypeIdList = params.merchType.split(',').collect {it.toLong()}
        outlet.merchTypes.findAll {merchTypeIdList.contains(it.id)} .each {
            SubReport subReport = new SubReport();
            subReport.merchType = it;

            List<EventJSONTemplate> events = eventService.getEventsForYear(it, null).sort {it.date}
            events = events.reverse()

            Date now = new Date().clearTime()
            Date plannedDate
            if(!events.empty){
                plannedDate = new Date(events.find {it.date.toLong() <= now.time} ?.date?.toLong() ?: events.last()?.date?.toLong()) //TODO: split the line
            } else {
                plannedDate = new Date()
            }
            subReport.plannedDate = plannedDate
            subReport.realDate = now

            report.addToSupReports subReport
        }

        return [report: report]
    }

    def show(Report reportInstance) {
        respond reportInstance
    }

    @Transactional
    def save() {
        Report report = new Report(
                outlet: Outlet.findByAddress(params.address),
                responsiblePerson: params.responsiblePerson
        )
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd")
        List<Integer> supposedToBeList = params.list('supposedToBe')?.collect {it.toInteger()}
        List<Integer> factList = params.list('fact')?.collect {it.toInteger()}
        List<Date> plannedDateList = params.list('plannedDate')?.collect {format.parse(it)}
        List<Date> realDateList = params.list('realDate')?.collect {format.parse(it)}
        params.list('merchType').eachWithIndex {merchType, i ->
            SubReport subReport = new SubReport(
                    merchType: MerchType.get(merchType.toLong()),
                    supposedToBe: supposedToBeList.get(i),
                    fact: factList.get(i),
                    plannedDate: plannedDateList.get(i),
                    realDate: realDateList.get(i)
            )
            report.addToSupReports subReport
        }
        report.save(flush: true)
        redirect(action: "show", id: report.id)
    }
}
