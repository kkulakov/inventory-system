package inventorysystem

import grails.transaction.Transactional

@Transactional(readOnly = true)
class RegistrationController {

    static allowedMethods = [save: "POST"]

    def index() {
        respond new User()
    }

    @Transactional
    def save(User userInstance){
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view: 'index'
            return
        }

        userInstance.save flush: true
        UserRole.create(userInstance, Role.findByAuthority("ROLE_USER"), true)

        redirect(controller: "login", action: "auth")
    }
}
