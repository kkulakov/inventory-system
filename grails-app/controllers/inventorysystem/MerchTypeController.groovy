package inventorysystem

import inventorysystem.enums.Repeat
import inventorysystem.utils.MerchTypeUtils

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MerchTypeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond MerchType.list(params), model:[merchTypeInstanceCount: MerchType.count()]
    }

    def show(MerchType merchTypeInstance) {
        respond merchTypeInstance
    }

    def create() {
        respond new MerchType(params)
    }

    @Transactional
    def save(MerchType merchTypeInstance) {
        //TODO: add some validation
        switch(params.repeat) {
            case "MONTHLY":
            case "YEARLY":
                merchTypeInstance.addToDates(new Date(params.datesstring))
                break
            case "DATES":
                MerchTypeUtils.stringToDateSet(params.datesstring).each {merchTypeInstance.addToDates(it)}
                break
        }

        if (merchTypeInstance == null) {
            notFound()
            return
        }

        if (merchTypeInstance.hasErrors()) {
            respond merchTypeInstance.errors, view:'create'
            return
        }

        merchTypeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'merchTypeInstance.label', default: 'MerchType'), merchTypeInstance.id])
                redirect merchTypeInstance
            }
            '*' { respond merchTypeInstance, [status: CREATED] }
        }
    }

    def edit(MerchType merchTypeInstance) {
        respond merchTypeInstance
    }

    @Transactional
    def update(MerchType merchTypeInstance) {
        //TODO: add some validation
        switch(params.repeat) {
            case "MONTHLY":
            case "YEARLY":
                merchTypeInstance?.dates?.clear()
                merchTypeInstance.addToDates(new Date(params.datesstring))
                break
            case "DATES":
                MerchTypeUtils.stringToDateSet(params.datesstring).each {merchTypeInstance.addToDates(it)}
                break
        }

        if (merchTypeInstance == null) {
            notFound()
            return
        }

        if (merchTypeInstance.hasErrors()) {
            respond merchTypeInstance.errors, view:'edit'
            return
        }

        merchTypeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'MerchType.label', default: 'MerchType'), merchTypeInstance.id])
                redirect merchTypeInstance
            }
            '*'{ respond merchTypeInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(MerchType merchTypeInstance) {

        if (merchTypeInstance == null) {
            notFound()
            return
        }

        merchTypeInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'MerchType.label', default: 'MerchType'), merchTypeInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'merchTypeInstance.label', default: 'MerchType'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
