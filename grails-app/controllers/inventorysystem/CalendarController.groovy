package inventorysystem

import inventorysystem.utils.EventJSONTemplate
import inventorysystem.enums.Repeat

import grails.converters.JSON

class CalendarController {
    EventService eventService

    def index() {
        int currentYear = new Date().year + 1900;
        return [year: currentYear, outletList: Outlet.list(), merchTypeList: MerchType.list()]
    }

    def events() {

        List<EventJSONTemplate> result = new ArrayList<>()

        List<Long> outletIdList = params.list("outlet[]").collect {it.toLong()}
        Outlet.findAllByIdInList(outletIdList).each {
            String description = it.address

            List<Long> merchTypeIdList = params.list("merchType[]").collect {it.toLong()}
            it.merchTypes.findAll { merchTypeIdList.contains(it.id) }.each {
                result.addAll(eventService.getEventsForYear(it, description))
            }
        }
        render(result as JSON)
    }
}
