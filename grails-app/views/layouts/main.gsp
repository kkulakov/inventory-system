<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Inventory System"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'datepicker3.css')}" type="text/css">
		<g:javascript src="jquery-1.11.1.min.js"/>
		<g:javascript src="bootstrap.min.js"/>
		<g:javascript src="bootstrap-datepicker.js"/>
		<g:javascript library="application"/>
        <g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>
        <div class="navbar" style="padding-top: 2%; padding-left: 1%;">
            <g:link controller="calendar" action="index" class="btn btn-primary btn-large">Графік інвентарізації</g:link>
            <g:link controller="merchType" action="index" class="btn btn-primary btn-large">Типи товарів</g:link>
            <g:link controller="outlet" action="index" class="btn btn-primary btn-large">Торгові точки</g:link>
            <g:link controller="report" action="index" class="btn btn-primary btn-large">Звіти</g:link>
            <g:link controller="logout" action="index" class="btn btn-primary btn-large">Вийти</g:link>
        </div>
		<g:layoutBody/>
		<div class="footer" role="contentinfo"></div>
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
		<r:layoutResources />
	</body>
</html>
