<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <meta name="layout" content="main">
        <title>Створити звіт</title>
        <script type="text/javascript">
            function changeSelection(checkboxClass, status){
                $(checkboxClass).each(function() {
                    this.checked = status;
                });
            }
            function createReport(link){
                 var outletValue = $('input[name=outlet]:checked').val();

                var merchTypeValues = $('input:checkbox:checked.merchType').map(function () {
                    return this.value;
                }).get();

                if(merchTypeValues == ''){
                    $("#emptyError").show("slow");
                    return;
                }

                window.location.href = (link + "?outlet=" + outletValue + "&merchType=" + merchTypeValues);
            }
        </script>
    </head>
    <body>
        <h3 align="center">Створення звіту по інвентарізації</h3>
        <div id="emptyError" class="alert alert-danger" hidden="true">
            Необхідно відмітити хоча б один тип товару!
        </div>
        <table class="table">
            <tr>
                <td width="50%">
                    <h4>Торгові точки:</h4>
                    <g:each in="${outletList}" var="outlet">
                        <div class="checkbox">
                            <label for="outlet">${outlet.address}</label>
                            <g:radio name="outlet" class="outlet" value="${outlet.id}" checked="true"/>
                        </div>
                    </g:each>
                </td>
                <td width="50%">
                    <h4>Типи товарів:</h4>
                    <g:each in="${merchTypeList}" var="merchType">
                        <div class="checkbox">
                            <label for="merchType">${merchType.name}</label>
                            <g:checkBox name="merchType" class="merchType" value="${merchType.id}" checked="true"/>
                        </div>
                    </g:each>
                    <button type="button" class="btn btn-success" onclick="changeSelection('.merchType', true)">Позначити</button>
                    <button type="button" class="btn btn-warning" onclick="changeSelection('.merchType', false)">Відмінити</button>
                </td>
            </tr>
        </table>
        <div align="center">
            <button type="button" class="btn btn-primary btn-lg" onclick="createReport('<g:createLink action="create" />')">
                Створити звіт
            </button>
        </div>
        <br>
    </body>
</html>