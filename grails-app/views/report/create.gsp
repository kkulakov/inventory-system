<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
      <meta name="layout" content="main">
      <title>Створити звіт</title>
</head>
<body>
<form action="<g:createLink action="save"/>" method="POST" role="form" style="padding-left: 5%; padding-right: 5%;">
    <div class="form-group">
        <label for="responsiblePerson">Відповідальна особа:</label>
        <input type="text" class="form-control" name="responsiblePerson" id="responsiblePerson" required
               value="${report.responsiblePerson}">
    </div>
    <div class="form-group">
        <label for="address">Адреса торгової точки:</label>
        <input type="text" class="form-control" name="address" id="address" required value="${report.outlet.address}">
    </div>
    <h3>Типи товарів:</h3>
    <table class="table table-bordered">
        <tr>
            <th>Товар
            <th>Повинно бути
            <th>Факт
            <th>Запланована дата
            <th>Фактична дата
        </tr>
        <g:each in="${report.supReports}" var="subReport">
            <tr>
                <td>
                    ${subReport.merchType.name}
                    <input type="hidden"" name="merchType" value="${subReport.merchType.id}" />
                </td>
                <td>
                    <input type="number" class="form-control" name="supposedToBe" required>
                </td>
                <td>
                    <input type="number" class="form-control" name="fact" required>
                </td>
                <td>
                    <input type="date" class="form-control" name="plannedDate" required
                           value="<g:formatDate format="yyyy-MM-dd" date="${subReport.plannedDate}"/>">
                </td>
                <td>
                    <input type="date" class="form-control" name="realDate" required
                           value="<g:formatDate format="yyyy-MM-dd" date="${subReport.realDate}"/>">
                </td>
            </tr>
        </g:each>
    </table>
    <br>
        <input class="btn btn-primary btn-lg" type="submit" value="Створити звіт">
    <br>
</form>
</body>
</html>