<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Звіт №${reportInstance.id}</title>
</head>
<body>
<div style="padding-left: 5%; padding-right: 5%;">
    <h2>Звіт №${reportInstance.id}</h2>
    <div class="form-group">
        <strong>Відповідальна особа:</strong> ${reportInstance.responsiblePerson}
    </div>
    <div class="form-group">
        <strong>Адреса торгової точки:</strong> ${reportInstance.outlet.address}
    </div>
    <h3>Типи товарів:</h3>
    <table class="table table-bordered">
        <tr>
            <th>Товар
            <th>Повинно бути
            <th>Факт
            <th>Запланована дата
            <th>Фактична дата
        </tr>
        <g:each in="${reportInstance.supReports}" var="subReportInstance">
            <tr>
                <td>
                    ${subReportInstance.merchType.name}
                </td>
                <td>
                    ${subReportInstance.supposedToBe}
                </td>
                <td>
                    ${subReportInstance.fact}
                </td>
                <td>
                    <g:formatDate format="dd.MM.yyyy" date="${subReportInstance.plannedDate}"/>
                </td>
                <td>
                    <g:formatDate format="dd.MM.yyyy" date="${subReportInstance.realDate}"/>
                </td>
            </tr>
        </g:each>
    </table>
    <br>
</div>
</body>
</html>