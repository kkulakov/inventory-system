
<%@ page import="inventorysystem.Report" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title>Звіти</title>
</head>
<body>
<a href="#list-report" class="skip" tabindex="-1">Перейти до вмісту&hellip;</a>
<div class="nav" role="navigation">
    <ul>
        <li><g:link class="btn btn-default" action="choose">Створити звіт</g:link></li>
    </ul>
</div>
<div id="list-report" class="content scaffold-list" role="main">
    <h1>Звіти:</h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="id" title="№" />

            <g:sortableColumn property="responsiblePerson" title="Відповідальна особа" />

            <g:sortableColumn property="outlet.address" title="Адресса торгової точки" />

        </tr>
        </thead>
        <tbody>
        <g:each in="${reportInstanceList}" status="i" var="reportInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show" id="${reportInstance.id}">${fieldValue(bean: reportInstance, field: "id")}</g:link></td>

                <td>${fieldValue(bean: reportInstance, field: "responsiblePerson")}</td>

                <td>${fieldValue(bean: reportInstance, field: "outlet.address")}</td>

            </tr>
        </g:each>
        </tbody>
    </table>
    <div class="pagination">
        <g:paginate total="${reportInstanceCount ?: 0}" />
    </div>
</div>
</body>
</html>
