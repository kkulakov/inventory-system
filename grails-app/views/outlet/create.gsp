<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title>Створити торгову точку</title>
	</head>
	<body>
		<a href="#create-outlet" class="skip" tabindex="-1">Перейти до вмісту&hellip;</a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="btn btn-default" action="index">Список торгових точок</g:link></li>
			</ul>
		</div>
		<div id="create-outlet" class="content scaffold-create" role="main">
			<h1>Створити торгову точку</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${outletInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${outletInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form url="[resource:outletInstance, action:'save']" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="Створити" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
