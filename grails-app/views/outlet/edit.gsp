<%@ page import="inventorysystem.Outlet" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title>Редагувати торгову точку</title>
	</head>
	<body>
		<a href="#edit-outlet" class="skip" tabindex="-1">Перейти до вмісту&hellip;</a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="btn btn-default" action="index">Список торгових точок</g:link></li>
				<li><g:link class="btn btn-default" action="create">Створити торгову точку</g:link></li>
			</ul>
		</div>
		<div id="edit-outlet" class="content scaffold-edit" role="main">
			<h1>Редагувати торгову точку</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${outletInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${outletInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form url="[resource:outletInstance, action:'update']" method="PUT" >
				<g:hiddenField name="version" value="${outletInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="save" action="update" value="Зберегти" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
