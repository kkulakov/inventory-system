
<%@ page import="inventorysystem.Outlet" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title>Список торгових точок</title>
	</head>
	<body>
		<a href="#list-outlet" class="skip" tabindex="-1">Перейти до вмісту&hellip;</a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="btn btn-default" action="create">Створити торгову точку</g:link></li>
			</ul>
		</div>
		<div id="list-outlet" class="content scaffold-list" role="main">
			<h1>Список торгових точок</h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="address" title="${message(code: 'outlet.address.label', default: 'Адреса')}" />
					
						<g:sortableColumn property="phoneNumber" title="${message(code: 'outlet.phoneNumber.label', default: 'Номер телефону')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${outletInstanceList}" status="i" var="outletInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${outletInstance.id}">${fieldValue(bean: outletInstance, field: "address")}</g:link></td>
					
						<td>${fieldValue(bean: outletInstance, field: "phoneNumber")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${outletInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
