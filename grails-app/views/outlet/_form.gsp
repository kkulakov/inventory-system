<%@ page import="inventorysystem.Outlet" %>



<div class="fieldcontain ${hasErrors(bean: outletInstance, field: 'address', 'error')} required">
	<label for="address">
		<g:message code="outlet.address.label" default="Адреса" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="address" required="" value="${outletInstance?.address}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: outletInstance, field: 'phoneNumber', 'error')} ">
	<label for="phoneNumber">
		<g:message code="outlet.phoneNumber.label" default="Номер телефону" />
		
	</label>
	<g:textField name="phoneNumber" value="${outletInstance?.phoneNumber}"/>

</div>
<g:if test="${merchTypeList}">
    <div class="fieldcontain "><label style="width: 50%;">Оберіть типи товарів, що знаходяться на даній торговій точці:</label></div>
    <g:each in="${merchTypeList}" var="merchType">
        <div class="fieldcontain ">
            <label for="merchType">${merchType.name}</label>
            <g:checkBox name="merchType" value="${merchType.id}" checked="${outletInstance?.merchTypes?.contains(merchType)}"/>
        </div>
    </g:each>
</g:if>

<br>
<br>