
<%@ page import="inventorysystem.Outlet" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title>Торгова точка</title>
	</head>
	<body>
		<a href="#show-outlet" class="skip" tabindex="-1">Перейти до вмісту&hellip;</a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="btn btn-default" action="index">Список торгових точок</g:link></li>
				<li><g:link class="btn btn-default" action="create">Створити торгову точку</g:link></li>
			</ul>
		</div>
		<div id="show-outlet" class="content scaffold-show" role="main">
			<h1>Торгова точка</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list outlet">
			
				<g:if test="${outletInstance?.address}">
				<li class="fieldcontain">
					<span id="address-label" class="property-label"><g:message code="outlet.address.label" default="Адреса" /></span>
					
						<span class="property-value" aria-labelledby="address-label"><g:fieldValue bean="${outletInstance}" field="address"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${outletInstance?.phoneNumber}">
				<li class="fieldcontain">
					<span id="phoneNumber-label" class="property-label"><g:message code="outlet.phoneNumber.label" default="Номер телефону" /></span>
					
						<span class="property-value" aria-labelledby="phoneNumber-label"><g:fieldValue bean="${outletInstance}" field="phoneNumber"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:outletInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${outletInstance}">Редагувати</g:link>
					<g:actionSubmit class="delete" action="delete" value="Видалити" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Ви впевнені?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
