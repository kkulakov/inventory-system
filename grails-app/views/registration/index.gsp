<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Реєстрація</title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css/custom', file: 'registration.css')}" type="text/css">
</head>
<body>
    <div class='container'>

        <g:hasErrors bean="${userInstance}">
            <div align="center" class="alert alert-danger">
                <ul>
                    <g:eachError bean="${userInstance}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                    </g:eachError>
                </ul>
            </div>
        </g:hasErrors>

        <g:form action="save" method="POST" class="form-signin">
            <h2 class="form-signin-heading">Будь ласка, заповніть всі поля</h2>
            <g:field type="email" name='email' id='email' class="form-control" placeholder="Email-адреса" required="" autofocus=""/>
            <g:field type="password" name='password' id='password' class="form-control" placeholder="Пароль" required=""/>
            <g:field type="text" name='firstName' id='firstName' class="form-control" placeholder="Ім'я" required=""/>
            <g:field type="text" name='lastName' id='lastName' class="form-control" placeholder="Прізвіще" required=""/>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Реєстрація</button>
        </g:form>

        <g:if test='${flash.message}'>
            <div align="center" class="alert alert-danger">${flash.message}</div>
        </g:if>
    </div>
</body>
</html>