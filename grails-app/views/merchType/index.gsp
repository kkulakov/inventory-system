
<%@ page import="inventorysystem.MerchType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'merchType.label', default: 'MerchType')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-merchType" class="skip" tabindex="-1">Перейти до вмісту&hellip;</a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="btn btn-default" action="create">Створити новий тип товарів</g:link></li>
			</ul>
		</div>
		<div id="list-merchType" class="content scaffold-list" role="main">
			<h1>Список типів товарів</h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'merchType.name.label', default: 'Ім\'я')}" />

						<g:sortableColumn property="repeat" title="${message(code: 'merchType.repeat.label', default: 'Періодичність інвентарізації')}" />

					</tr>
				</thead>
				<tbody>
				<g:each in="${merchTypeInstanceList}" status="i" var="merchTypeInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${merchTypeInstance.id}">${fieldValue(bean: merchTypeInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: merchTypeInstance, field: "repeat")}</td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${merchTypeInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
