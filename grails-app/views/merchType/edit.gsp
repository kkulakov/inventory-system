<%@ page import="inventorysystem.MerchType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title>Редагувати тип товарів</title>
        <g:javascript src="custom/typesForm.js"/>
	</head>
	<body>
		<a href="#edit-merchType" class="skip" tabindex="-1">Перейти до вмісту&hellip;</a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="btn btn-default" action="index">Список типів товарів</g:link></li>
				<li><g:link class="btn btn-default" action="create">Створити тип товарів</g:link></li>
			</ul>
		</div>
		<div id="edit-merchType" class="content scaffold-edit" role="main">
			<h1>Редагувати тип товарів</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${merchTypeInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${merchTypeInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form url="[resource:merchTypeInstance, action:'update']" method="PUT" id="typeform" >
				<g:hiddenField name="version" value="${merchTypeInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="save" action="update" value="Зберегти" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
