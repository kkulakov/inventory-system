<%@ page import="inventorysystem.MerchType" %>

<div id="dayerror" class="alert alert-danger" hidden="true">Необхідно ввести кількість днів!</div>
<div id="dateserror" class="alert alert-danger" hidden="true">Необхідно обрати дату на календарі!</div>

<div class="fieldcontain ${hasErrors(bean: merchTypeInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="merchType.name.label" default="Ім'я" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${merchTypeInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: merchTypeInstance, field: 'repeat', 'error')} required">
	<label for="repeat">
		<g:message code="merchType.repeat.label" default="Періодичність інвентарізації" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="repeat" from="${inventorysystem.enums.Repeat?.values()}" keys="${inventorysystem.enums.Repeat.values()*.name()}" required="" value="${merchTypeInstance?.repeat?.name()}" />

</div>

<div id="interval" class="fieldcontain ${hasErrors(bean: merchTypeInstance, field: 'daysInterval', 'error')} ">
	<label for="daysInterval">
		<g:message code="merchType.daysInterval.label" default="Інтервал днів" />
		
	</label>
	<g:field name="daysInterval" type="number" value="${merchTypeInstance.daysInterval}"/>

</div>

<div id="singlepicker" class="fieldcontain " hidden="true">
    <label>Починаючи з </label>
    <div id="singlepickerCalendar" style="margin-top: -30px; margin-left: 25%;" class="fieldcontain "></div>
</div>
<div id="multipicker" class="fieldcontain " hidden="true">
    <label>Оберіть потрібні дати </label>
    <div id="multipickerCalendar" style="margin-top: -30px; margin-left: 25%;" class="fieldcontain "></div>
</div>

<input type="hidden" name="datesstring" id="datesstring" >
<br>
<br>