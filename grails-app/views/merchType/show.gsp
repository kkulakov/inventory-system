
<%@ page import="inventorysystem.MerchType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title>Тип товарів</title>
	</head>
	<body>
		<a href="#show-merchType" class="skip" tabindex="-1">Перейти до вмісту&hellip;</a>
		<div class="nav" role="navigation">
			<ul>
                <li><g:link class="btn btn-default" action="index">Список типів товарів</g:link></li>
                <li><g:link class="btn btn-default" action="create">Створити тип товарів</g:link></li>
			</ul>
		</div>
		<div id="show-merchType" class="content scaffold-show" role="main">
			<h1>Тип товарів</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list merchType">
			
				<g:if test="${merchTypeInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="merchType.name.label" default="Ім'я" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${merchTypeInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${merchTypeInstance?.repeat}">
				<li class="fieldcontain">
					<span id="repeat-label" class="property-label"><g:message code="merchType.repeat.label" default="Періодичність інвентарізації" /></span>
					
						<span class="property-value" aria-labelledby="repeat-label"><g:fieldValue bean="${merchTypeInstance}" field="repeat"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${merchTypeInstance?.daysInterval}">
				<li class="fieldcontain">
					<span id="daysInterval-label" class="property-label"><g:message code="merchType.daysInterval.label" default="Інтервал днів" /></span>
					
						<span class="property-value" aria-labelledby="daysInterval-label"><g:fieldValue bean="${merchTypeInstance}" field="daysInterval"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${merchTypeInstance?.dates}">
				<li class="fieldcontain">
					<span id="dates-label" class="property-label"><g:message code="merchType.dates.label" default="Дати" /></span>
					
						<span class="property-value" aria-labelledby="dates-label"><g:fieldValue bean="${merchTypeInstance}" field="dates"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:merchTypeInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${merchTypeInstance}">Редагувати</g:link>
					<g:actionSubmit class="delete" action="delete" value="Видалити" onclick="return confirm('Ви впевнені?');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
