<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main">
        <title>Календар інвентарізації</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'eventCalendar.css')}" type="text/css">
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'eventCalendar_theme_responsive.css')}" type="text/css">
        <g:javascript src="jquery.timeago.js"/>
        <g:javascript src="jquery.eventCalendar.min.js"/>
        <script type="text/javascript">
            $(document).ready(function(){

                    drawCalendar();

            });
            function drawCalendar(){

                var outletValues = $('input:checkbox:checked.outlet').map(function () {
                    return this.value;
                }).get();

                var merchTypeValues = $('input:checkbox:checked.merchType').map(function () {
                    return this.value;
                }).get();
                var jsonCheckboxValues = {
                    "outlet": outletValues,
                    "merchType": merchTypeValues
                }

                $.get('<g:createLink action="events" />', jsonCheckboxValues, function(jsonArray){
                    //clearing events and div
                    $('#calendar').off( "click", ".eventsCalendar-day a");
                    $('#calendar').off( "click", ".monthTitle");
                    $('#calendar').find('.eventsCalendar-list').off( "click", ".eventTitle");
                    $('#calendar').html('');


                    $('#calendar').eventCalendar({
                        jsonData: jsonArray,
                        eventsLimit: 10,
                        openEventInNewWindow: true,
                        showDescription: true,
                        monthNames: [ "Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень",
                            "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень" ],
                        dayNames: [ 'Неділя','Понеділок','Вівторок','Середа','Четвер',
                            'П\'ятниця','Субота' ],
                        dayNamesShort: [ 'Вс','Пн','Вт','Ср','Чт', 'Пт','Сб' ],
                        txt_noEvents: "На даний період перевірки не були заплановано",
                        txt_SpecificEvents_prev: "",
                        txt_SpecificEvents_after: "події:",
                        txt_next: "наступнмй",
                        txt_prev: "попередній",
                        txt_NextEvents: "Майбутні події:",
                        txt_GoToEventUrl: "До події"
                    });
                }); //TODO: add fail action
            }

            function changeSelection(checkboxClass, status){
                $(checkboxClass).each(function() {
                    this.checked = status;
                });
            }

        </script>
    </head>
    <body>
        <h3 align="center">Графік інвентарізації на ${year} рік</h3>
        <table class="table">
            <tr>
                <td width="50%">
                    <div id="calendar" align = "center"></div>
                </td>
                <td width="50%">
                    <table class="table">
                        <tr>
                            <td colspan="2">
                                <button type="button" class="btn btn-primary btn-lg btn-block" onclick="drawCalendar()">
                                    Оновити
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td width="50%">
                                <h4>Торгові точки:</h4>
                                <g:each in="${outletList}" var="outlet">
                                    <div class="checkbox">
                                        <label for="outlet">${outlet.address}</label>
                                        <g:checkBox name="outlet" class="outlet" value="${outlet.id}" checked="true"/>
                                    </div>
                                </g:each>
                                <button type="button" class="btn btn-success" onclick="changeSelection('.outlet', true)">Позначити</button>
                                <button type="button" class="btn btn-warning" onclick="changeSelection('.outlet', false)">Відмінити</button>
                            </td>
                            <td width="50%">
                                <h4>Типи товарів:</h4>
                                <g:each in="${merchTypeList}" var="merchType">
                                    <div class="checkbox">
                                        <label for="merchType">${merchType.name}</label>
                                        <g:checkBox name="merchType" class="merchType" value="${merchType.id}" checked="true"/>
                                    </div>
                                </g:each>
                                <button type="button" class="btn btn-success" onclick="changeSelection('.merchType', true)">Позначити</button>
                                <button type="button" class="btn btn-warning" onclick="changeSelection('.merchType', false)">Відмінити</button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>