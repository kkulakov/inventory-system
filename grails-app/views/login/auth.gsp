<html>
<head>
    <title>Авторизація</title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css/custom', file: 'login.css')}" type="text/css">
</head>

<body>
    <div class='container'>

        <form action='${postUrl}' method='POST' id='loginForm' class="form-signin" role="form">
            <h2 class="form-signin-heading">Будь ласка, авторизуйтесь</h2>
            <input type="email" name='j_username' id='username' class="form-control" placeholder="Email-адреса" required autofocus>
            <input type="password" name='j_password' id='password' class="form-control" placeholder="Пароль" required>
            <label class="checkbox">
                <input type="checkbox" name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/> Запам'ятати мене
            </label>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Вхід</button>
            <g:link controller="registration" action="index" class="btn btn-lg btn-success btn-block" type="submit">Реєстрація</g:link>
        </form>

        <g:if test='${flash.message}'>
            <div align="center" class="alert alert-danger">${flash.message}</div>
        </g:if>
    </div>
    <script type='text/javascript'>
        <!--
        (function() {
            document.forms['loginForm'].elements['j_username'].focus();
        })();
        // -->
    </script>
</body>
</html>
