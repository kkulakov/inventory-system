$(document).ready(function(){
    $('#singlepickerCalendar').datepicker({
        todayBtn: "linked",
        todayHighlight: true
    });
    $('#multipickerCalendar').datepicker({
        todayBtn: "linked",
        todayHighlight: true,
        multidate: true
    });

    $('#singlepickerCalendar').datepicker().on('changeDate', function(e){
        $('#datesstring').val($('#singlepickerCalendar').datepicker('getDate'));
    });

    $('#multipickerCalendar').datepicker().on('changeDate', function(e){
        $('#datesstring').val($('#multipickerCalendar').datepicker('getDates'));
    });

    var pickerSelect = function(event) {
        if(!this.value) {
            this.value = $('#repeat').val();
        }
        if (this.value == 'DAY_INTERVAL') {
            $('#datesstring').val('');

            $('#singlepicker').hide("slow");
            $('#multipicker').hide("slow");
            $('#interval').show("slow");
        } else if (this.value == 'MONTHLY' || this.value == 'YEARLY') {
            $('#datesstring').val($('#singlepickerCalendar').datepicker('getDate'));

            $('#multipicker').hide("slow");
            $('#interval').hide("slow");
            $('#singlepicker').show("slow");
        } else if (this.value == 'DATES') {
            $('#datesstring').val($('#multipickerCalendar').datepicker('getDates'));

            $('#singlepicker').hide("slow");
            $('#interval').hide("slow");
            $('#multipicker').show("slow");
        }
    }


    pickerSelect();
    $('#repeat').on('change' , pickerSelect);
    $( "#typeform" ).submit(function( event ) {
        if($('select').val() == 'DAY_INTERVAL' && $('#daysInterval').val() == '') {
            $('#dateserror').hide("slow");
            $('#dayerror').show("slow");
            event.preventDefault();
            return;
        }
        if(($('select').val() != 'DAY_INTERVAL' &&
            ($('#datesstring').val() == '' || $('#datesstring').val() == 'Invalid Date'))) {
            $('#dayerror').hide("slow");
            $('#dateserror').show("slow");
            event.preventDefault();
            return;
        }
    });
});
