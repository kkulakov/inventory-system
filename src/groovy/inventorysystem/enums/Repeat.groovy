package inventorysystem.enums

public enum Repeat {
    DAY_INTERVAL('Через певну кількість днів'),
    MONTHLY('Раз в місяць'),
    YEARLY('Раз в рік'),
    DATES('В певні дати')

    final String value
    Repeat(String value) {
        this.value = value
    }

    String toString() {
        value
    }
}